package com.gps.fleet.util;

import android.content.SharedPreferences;

import com.gps.fleet.App;
import com.gps.fleet.activity.SplashScreen;
import com.gps.fleet.pojo.PreferencePojo;

import static android.content.Context.MODE_PRIVATE;


public class PreferenceUtil {

    public static void setPreference(PreferencePojo preferencePojo) {
        SharedPreferences.Editor editor = SplashScreen.getEditor();

        editor.putString("username", preferencePojo.getUsername());
        editor.putString("password", preferencePojo.getPassword());
        editor.putString("flag", preferencePojo.getFlag());
        editor.putString("authcode", preferencePojo.getAuthcode());
        editor.putString("trackpermission", preferencePojo.getTrackpermission());
        editor.putString("reportpermission", preferencePojo.getReportpermission());
        editor.putString("checkstate", preferencePojo.getCheckState());

        editor.commit();
    }

    public static PreferencePojo getPreference() {
        SharedPreferences pref = App.getContext().getSharedPreferences("DataStore", MODE_PRIVATE);
        PreferencePojo preferencePojo = new PreferencePojo();

        preferencePojo.setUsername(pref.getString("username", ""));
        preferencePojo.setPassword(pref.getString("password", ""));
        preferencePojo.setFlag(pref.getString("flag", "0"));
        preferencePojo.setAuthcode(pref.getString("authcode", ""));
        preferencePojo.setTrackpermission(pref.getString("trackpermission", "false"));
        preferencePojo.setReportpermission(pref.getString("reportpermission", "false"));
        preferencePojo.setCheckState(pref.getString("checkstate", "false"));

        return preferencePojo;
    }
}
