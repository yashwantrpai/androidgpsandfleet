package com.gps.fleet;

import android.content.Context;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class App extends android.support.multidex.MultiDexApplication {
    public static final String PREFS_NAME = "logPrefFile";
    public static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        appContext = this;
    }

    public static Context getContext(){
        return appContext;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}