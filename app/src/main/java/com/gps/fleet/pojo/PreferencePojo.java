package com.gps.fleet.pojo;

public class PreferencePojo {

    String username;
    String password;
    String flag;
    String authcode;
    String trackpermission;
    String reportpermission;
    String checkState;

    public PreferencePojo() {
        this.username = "";
        this.password = "";
        this.flag = "0";
        this.authcode = "";
        this.trackpermission = "false";
        this.reportpermission = "false";
        this.checkState = "false";
    }

    public PreferencePojo(String username, String password, String flag, String authcode, String trackpermission, String reportpermission) {
        this.username = username;
        this.password = password;
        this.flag = flag;
        this.authcode = authcode;
        this.trackpermission = trackpermission;
        this.reportpermission = reportpermission;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAuthcode() {
        return authcode;
    }

    public void setAuthcode(String authcode) {
        this.authcode = authcode;
    }

    public String getTrackpermission() {
        return trackpermission;
    }

    public void setTrackpermission(String trackpermission) {
        this.trackpermission = trackpermission;
    }

    public String getReportpermission() {
        return reportpermission;
    }

    public void setReportpermission(String reportpermission) {
        this.reportpermission = reportpermission;
    }

    public String getCheckState() {
        return checkState;
    }

    public void setCheckState(String checkState) {
        this.checkState = checkState;
    }
}
