package com.gps.fleet.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gps.fleet.CreatePostString;
import com.gps.fleet.CustomHttpClient;
import com.gps.fleet.R;
import com.gps.fleet.pojo.PreferencePojo;
import com.gps.fleet.util.PreferenceUtil;
import com.gps.fleet.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    Button login;
    EditText username, password;
    String response = "true";
    private static final String TAG = "MyActivity";
    Context context;
    private ProgressDialog pdia;
    public SharedPreferences pref;
    public SharedPreferences.Editor editor;
    // public com.suke.widget.SwitchButton keep_session;
    public static Activity activity;
    public static TextView version;
    public static CheckBox sessionCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Create new file with new values
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        pref = context.getSharedPreferences("DataStore", MODE_PRIVATE);
        editor = pref.edit();

        activity = this;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color
            window.setStatusBarColor(ContextCompat.getColor(activity, R.color.colorgpsandfleetDark));
        }

        username = (EditText) findViewById(R.id.username);
        username.setHint((pref.getString("language", "0").equals("0")) ? R.string.username : R.string.username_esp);
        password = (EditText) findViewById(R.id.password);
        password.setHint((pref.getString("language", "0").equals("0")) ? R.string.password : R.string.password_esp);
        version = (TextView) findViewById(R.id.version);
        sessionCheck = findViewById(R.id.keep_session);
        TextView remember = findViewById(R.id.remember);
        // Terminate session if switch is off and app is launched from background
        // Display username in login page if switch is on
        if (getIntent() != null) {
            if (getIntent().getExtras() != null) {
                if (getIntent().getStringExtra("logout").equals("sessionUncheck")) {
                    sessionCheck.setChecked(false);
                }
            }
        }
        if (sessionCheck.isChecked()) {
            if (pref.getString("checkstate", "") != null) {
                if (pref.getString("checkstate", "").equals("true")) {
                    if (pref.getString("username", "") != null || pref.getString("password", "") != null) {
                        username.setText(pref.getString("username", ""));
                        password.setText(pref.getString("password", ""));
                    }
                } else {
                    username.setText("");
                    password.setText("");
                }
            }
        } else {
            username.setText("");
            password.setText("");
        }


        sessionCheck.setOnClickListener(new CheckBox.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!sessionCheck.isChecked()) {
                    editor.putString("checkstate", "false");
                    editor.apply();
                } else {
                    if (pref.getString("flag", "") != null) {
                        if (pref.getString("flag", "").equals("1")) {
                            if (pref.getString("username", "") != null || pref.getString("password", "") != null) {
                                username.setText(pref.getString("username", ""));
                                password.setText(pref.getString("password", ""));
                            }
                        }
                    }
                }
            }
        });

        login = (Button) findViewById(R.id.signin);
        login.setText((pref.getString("language", "0").equals("0")) ? R.string.login : R.string.login_esp);
        remember.setText((pref.getString("language", "0").equals("0")) ? R.string.remember_me : R.string.rememberme_esp);
        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!username.getText().toString().equals("") && !password.getText().toString().equals("")) {
                    ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                    if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                        login();
                    } else {
                        Toast.makeText(context, (pref.getString("language", "0").equals("0")) ? R.string.internet_connectivity_check : R.string.internet_connectivity_check_esp, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialogBuilder.setTitle((pref.getString("language", "0").equals("0")) ? R.string.empty_credentials : R.string.empty_credentials_esp);
                    alertDialogBuilder.setMessage((pref.getString("language", "0").equals("0")) ? R.string.enter_credentials_to_login : R.string.enter_credentials_to_login_esp);
                    alertDialogBuilder.setNeutralButton((pref.getString("language", "0").equals("0") ? R.string.ok : R.string.ok_esp),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialogBuilder.show();
                }
            }
        });
    }

    public void login() {
        String urls[] = getResources().getStringArray(R.array.url);
        String currentServer = pref.getString("currentServer", "");
        if (Util.isValidUrl(currentServer, urls)) {
            String loginUrl = currentServer + ((pref.getString("language", "0").equals("0")) ? getString(R.string.login_url) : getString(R.string.login_url_esp));
            Log.d("login url", loginUrl);
            ArrayList params = new ArrayList();
            Pair pair = new Pair("operation", "login");
            params.add(pair);
            pair = new Pair("username", username.getText().toString());
            params.add(pair);
            pair = new Pair("password", password.getText().toString()); // android.telephony.TelephonyManager.getDeviceId()
            params.add(pair);

            String postString = CreatePostString.createPostString(params);
            Log.i("param", postString);
            loginTask loginTask = new loginTask();
            loginTask.execute(loginUrl, postString);
        } else {
            Toast.makeText(context, (pref.getString("language", "0").equals("0")) ? R.string.server_connection_issue : R.string.server_connection_issue_esp, Toast.LENGTH_SHORT).show();
            Intent i = new Intent(context, SplashScreen.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (pdia != null && pdia.isShowing()) {
            pdia.cancel();
        }
    }

    private class loginTask extends AsyncTask<String, Void, String> implements DialogInterface.OnCancelListener {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!isFinishing() && android.os.Build.VERSION.SDK_INT > 19) {
                pdia = new ProgressDialog(context);
                pdia.setMessage((pref.getString("language", "0").equals("0")) ? getString(R.string.validating) : getString(R.string.validating_esp));
                pdia.setCancelable(false);
                pdia.show();
            }
        }

        @Override
        protected String doInBackground(String... url) {
            // TODO Auto-generated method stub
            String res = null;
            try {
                response = CustomHttpClient.executeHttpPost(url[0], url[1]);
                res = response.toString();
                Log.d("Response", res);
                //res= res.replaceAll("\\s+","");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            if (!isFinishing()) {
                if (pdia != null && pdia.isShowing()) {
                    pdia.dismiss();
                }
            }
            response = result;

            if (response != null && !response.equals("")) {
                Log.d(TAG, "Enable_login response: " + response);
                JSONObject respjson = new JSONObject();
                try {
                    respjson = new JSONObject(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    //Log.d(TAG, response);
                    if (response != null && response.contains("true")) {
                        JSONObject data = new JSONObject();
                        try {
                            data = respjson.getJSONObject("data");
                            String authcode = data.getString("authcode");
                            String trackpermission = data.getString("trackpermission");
                            String reportpermission = data.getString("reportpermission");
                            /* if(pref.getString("keepSession", "0").equals("1")){
                                editor.putString("savedUsername", email.getText().toString());
                                editor.putString("savedPassword", passcode.getText().toString());
                                editor.commit();
                            } */

                            PreferencePojo preferencePojo = new PreferencePojo();
                            preferencePojo.setReportpermission(reportpermission);
                            preferencePojo.setTrackpermission(trackpermission);
                            preferencePojo.setAuthcode(authcode);
                            preferencePojo.setUsername(username.getText().toString());
                            preferencePojo.setPassword(password.getText().toString());
                            preferencePojo.setFlag("1");
                            if (sessionCheck.isChecked()) {
                                preferencePojo.setCheckState("true");
                            }
                            PreferenceUtil.setPreference(preferencePojo);

                            Intent i = new Intent(MainActivity.this, HomeActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        AlertDialog WrongPasswordAlert = new AlertDialog.Builder(MainActivity.this).create();
                        WrongPasswordAlert.setTitle((pref.getString("language", "0").equals("0")) ? R.string.login_failed : R.string.login_failed_esp);
                        WrongPasswordAlert.setMessage((pref.getString("language", "0").equals("0")) ? getString(R.string.input_correct_credentials) : getString(R.string.input_correct_credentials_esp));
                        WrongPasswordAlert.setButton(AlertDialog.BUTTON_NEUTRAL, activity.getResources().getString((pref.getString("language", "0").equals("0")) ? R.string.ok : R.string.ok_esp),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        WrongPasswordAlert.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }//close onPostExecute
        }// close loginTask

        @Override
        public void onCancel(DialogInterface dialog) {
            cancel(true);
        }
    }
}
