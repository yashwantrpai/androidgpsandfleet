package com.gps.fleet.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.gps.fleet.CreatePostString;
import com.gps.fleet.CustomHttpClient;
import com.gps.fleet.R;
import com.gps.fleet.activity.HomeActivity;

import java.util.ArrayList;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class help extends Fragment {
    Switch loggingSwitch;
    public static final String PREFS_NAME = "logPrefFile";
    public SharedPreferences pref;
    public SharedPreferences.Editor editor;
    public Activity activity;
    ArrayList<Integer> Image_icon = new ArrayList<>();
    ArrayList<String> text = new ArrayList<>();
    ArrayList<Integer> Image = new ArrayList<>();
    RecyclerView Alert_recycler_View;
    SettingsAdapter Adapter;
    public AlertDialog.Builder ratingAlert, selectLanguageAlert;
    public AlertDialog ratingDialog, languageSelectDialog;
    public View ratingView, languageSelectView;
    public LinearLayout ratingLayout;
    public RadioGroup radioGroup;
    public String response;
    public boolean isLanguageModified = false;

    public help() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_help, container, false);
        activity = getActivity();
        pref = activity.getSharedPreferences("DataStore", MODE_PRIVATE);
        editor = pref.edit();

        Image_icon.clear();
        text.clear();
        Image.clear();

        Alert_recycler_View = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        Adapter = new SettingsAdapter(Image_icon, text, Image);
        Alert_recycler_View.setAdapter(Adapter);
        Alert_recycler_View.setHasFixedSize(true);
        Alert_recycler_View.setLayoutManager(new LinearLayoutManager(getActivity()));

        setNewLayout(R.drawable.languageselect, getString((pref.getString("language", "0").equals("0")) ? R.string.change_language : R.string.change_language_esp), R.drawable.next);
        setNewLayout(R.drawable.changepassword, getString((pref.getString("language", "0").equals("0")) ? R.string.change_password : R.string.change_password_esp), R.drawable.next);
        setNewLayout(R.drawable.mailus, getString((pref.getString("language", "0").equals("0")) ? R.string.contact_us : R.string.contact_us_esp), R.drawable.next);
        setNewLayout(R.drawable.rateus, getString((pref.getString("language", "0").equals("0")) ? R.string.rate_us : R.string.rate_us_esp), R.drawable.next);

        return rootView;
    }

    public void sendMail() {
        editor.putString("CURRENT_TAG", HomeActivity.TAG_HELP);
        editor.commit();
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "support@tracking-gpsandtrack.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "GPS and Fleet Support mail");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        startActivity(Intent.createChooser(emailIntent, (pref.getString("language", "0").equals("0")) ? getString(R.string.send_mail) : getString(R.string.send_mail_esp)));
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void inflateRatingLayout() {
        editor.putString("CURRENT_TAG", HomeActivity.TAG_HELP);
        editor.commit();
        LayoutInflater inflater = getActivity().getLayoutInflater();
        ratingView = inflater.inflate(R.layout.ratinglayout, null);
        ratingLayout = (LinearLayout) ratingView.findViewById(R.id.rating_layout);
        ratingLayout.setVisibility(View.VISIBLE);
        final TextView rating_layout_title = (TextView) ratingView.findViewById(R.id.rating_layout_title);
        rating_layout_title.setText((pref.getString("language", "0").equals("0")) ? R.string.rate_us : R.string.rate_us_esp);
        final TextView rate_me = (TextView) ratingView.findViewById(R.id.rate_me);
        rate_me.setText((pref.getString("language", "0").equals("0")) ? R.string.rating_text : R.string.rating_text_esp);
        final ImageView close = (ImageView) ratingView.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ratingDialog.dismiss();
            }
        });
        final RatingBar ratingBar = (RatingBar) ratingView.findViewById(R.id.ratingBar);
        final EditText ratingText = (EditText) ratingView.findViewById(R.id.ratingText);
        ratingText.setHint((pref.getString("language", "0").equals("0")) ? R.string.review_hint : R.string.review_hint_esp);
        final Button submitRating = (Button) ratingView.findViewById(R.id.submit);
        submitRating.setText((pref.getString("language", "0").equals("0")) ? R.string.submit : R.string.submit_esp);

        ratingAlert = new AlertDialog.Builder(activity);
        // this is set the view from XML inside AlertDialog
        ratingAlert.setView(ratingView);
        // disallow cancel of AlertDialog on click of back button and outside touch
        ratingAlert.setCancelable(false);
        ratingDialog = ratingAlert.create();

        submitRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float rating = ratingBar.getRating();
                String reviewText = ratingText.getText().toString();
                System.out.println("rate star" + rating);
                System.out.println("rate text" + reviewText);
                if (!reviewText.isEmpty() && rating != 0.0) {
                    submitReview(rating, reviewText);
                } else {
                    ratingDialog.dismiss();
                    Toast.makeText(getActivity(), "Unable to send the feedback", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ratingDialog.show();
    }

    public void submitReview(float rating, String review) {
        SharedPreferences pref;
        pref = activity.getSharedPreferences("DataStore", Context.MODE_PRIVATE);
        String currentServer = pref.getString("currentServer", "");
        String sendUserFeedbackUrl = currentServer + ((pref.getString("language", "0").equals("0")) ? getString(R.string.star_rating_url) : getString(R.string.star_rating_url_esp));

        ArrayList params = new ArrayList();
        Pair pair = new Pair("operation", "SubmitRatings");
        params.add(pair);
        pair = new Pair("authcode", pref.getString("authcode", ""));
        params.add(pair);
        pair = new Pair("ratings", rating);
        params.add(pair);
        pair = new Pair("comments", review);
        params.add(pair);

        String postString = CreatePostString.createPostString(params);
        sendUserFeedbackTask sendUserFeedbackTask = new sendUserFeedbackTask();
        sendUserFeedbackTask.execute(sendUserFeedbackUrl, postString);
    }

    public class sendUserFeedbackTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            // TODO Auto-generated method stub
            String res = null;
            try {
                response = CustomHttpClient.executeHttpPost(url[0], url[1]);
                res = response.toString();
            } catch (Exception e) {

            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            Boolean responsefailed = false;

            if (result != null && result != "" && !result.isEmpty()) {
                response = result;
                Log.d("sendFeedback", response);

                if (response.contains("success")) {
                    Log.d("sendFeedback", "Feedback sent successfully");
                    if (ratingDialog.isShowing()) {
                        ratingLayout.setVisibility(View.GONE);
                        LinearLayout thanksLayout = (LinearLayout) ratingView.findViewById(R.id.thanks_layout);
                        thanksLayout.setVisibility(View.VISIBLE);
                        TextView thank_you_text = (TextView) ratingView.findViewById(R.id.thanksText);
                        thank_you_text.setText((pref.getString("language", "0").equals("0")) ? R.string.thank_you_text : R.string.thank_you_text_esp);
                        Button backBtn = ratingView.findViewById(R.id.backBtn);
                        backBtn.setText((pref.getString("language", "0").equals("0")) ? R.string.back : R.string.back_esp);
                        backBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ratingDialog.dismiss();
                            }
                        });
                    }
                } else {
                    responsefailed = true;
                }
            } else {
                responsefailed = true;
            }

            if (responsefailed) {
                if (ratingDialog.isShowing()) {
                    ratingDialog.dismiss();
                }
                Toast.makeText(activity, (pref.getString("language", "0").equals("0")) ? R.string.send_feedback : R.string.send_feedback_esp, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void setNewLayout(int image_icon, String txt, int image) {
        Image_icon.add(image_icon);
        text.add(txt);
        Image.add(image);

        Adapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();

        SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
        boolean isLogEnabled = settings.getBoolean("logEnabled", false);
        Log.i("help", "### onResume: " + isLogEnabled);
        if (isLogEnabled) {
            if (loggingSwitch != null) {
                loggingSwitch.setChecked(isLogEnabled);
            }
        }
    }

    public class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.MyViewHolder> {

        ArrayList<Integer> Image_icon = new ArrayList<>();
        ArrayList<String> Text = new ArrayList<>();
        ArrayList<Integer> Image = new ArrayList<>();

        public SettingsAdapter(ArrayList<Integer> Image_icon, ArrayList<String> text, ArrayList<Integer> Image) {
            this.Image_icon = Image_icon;
            this.Text = text;
            this.Image = Image;
        }

        @Override
        public SettingsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View card_view = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewlayout, parent, false);
            return new SettingsAdapter.MyViewHolder(card_view);
        }

        @Override
        public void onBindViewHolder(SettingsAdapter.MyViewHolder holder, final int position) {
            try {
                holder.text.setText(Text.get(position).toString());
                holder.image_icon.setImageResource(Image_icon.get(position));
                holder.image.setImageResource(Image.get(position));

                holder.image.setOnClickListener(new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onClick(View view) {
                        action(position);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return Image_icon.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView text;
            ImageView image_icon, image;

            public MyViewHolder(View view) {
                super(view);
                image_icon = (ImageView) view.findViewById(R.id.image_icon);
                text = (TextView) view.findViewById(R.id.text);
                image = (ImageView) view.findViewById(R.id.next);
            }
        }
    }

    public void inflateLanguageSelectLayout() {
        editor.putString("CURRENT_TAG", HomeActivity.TAG_HELP);
        editor.commit();
        LayoutInflater inflater = activity.getLayoutInflater();
        languageSelectView = inflater.inflate(R.layout.language_select_layout, null);
        radioGroup = (RadioGroup) languageSelectView.findViewById(R.id.radioGroup);
        final TextView languageSelectTitle = languageSelectView.findViewById(R.id.select_language_layout_title);
        final RadioButton english = (RadioButton) languageSelectView.findViewById(R.id.english);
        final RadioButton spanish = (RadioButton) languageSelectView.findViewById(R.id.spanish);
        Button submit = (Button) languageSelectView.findViewById(R.id.submit);
        submit.setText((pref.getString("language", "0").equals("0")) ? R.string.save : R.string.save_esp);
        final ImageView close = (ImageView) languageSelectView.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                languageSelectDialog.dismiss();
            }
        });

        languageSelectTitle.setText((pref.getString("language", "0").equals("0")) ? R.string.change_language : R.string.change_language_esp);

        selectLanguageAlert = new AlertDialog.Builder(activity);
        // this is set the view from XML inside AlertDialog
        selectLanguageAlert.setView(languageSelectView);
        // disallow cancel of AlertDialog on click of back button and outside touch
        selectLanguageAlert.setCancelable(false);
        languageSelectDialog = selectLanguageAlert.create();

        languageSelectDialog.show();
        if (pref.getString("language", "0").equals("1")) {
            radioGroup.check(R.id.spanish);
        } else {
            radioGroup.check(R.id.english);
        }

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                languageSelectDialog.dismiss();
                int checkedId = radioGroup.getCheckedRadioButtonId();
                int checked = Integer.parseInt(pref.getString("language", "0"));
                int checked_btn = R.id.english;
                if (checked == 0) {
                    checked_btn = R.id.english;
                } else {
                    checked_btn = R.id.spanish;
                }

                if (checked_btn != checkedId) {
                    switch (checkedId) {
                        case R.id.english:
                            editor.putString("language", "0");
                            editor.apply();
                            break;

                        case R.id.spanish:
                            editor.putString("language", "1");
                            editor.apply();
                            break;
                    }

                    Intent intent = getActivity().getIntent();
                    getActivity().finish();
                    startActivity(intent);
                }
            }
        });
    }

    public void loadChangePasswordFragment() {
        String CURRENT_TAG = HomeActivity.TAG_CHANGE_PASSWORD;
        Fragment targetFragment = new changePassword();
        //setToolbarTitle();
        editor.putString("CURRENT_TAG", HomeActivity.TAG_CHANGE_PASSWORD);
        editor.commit();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        fragmentTransaction.replace(R.id.frame, targetFragment, CURRENT_TAG);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void action(int position) {
        switch (position) {
            case 0:
                inflateLanguageSelectLayout();
                break;

            case 1:
                loadChangePasswordFragment();
                break;

            case 2:
                sendMail();
                break;

            case 3:
                inflateRatingLayout();
                break;

            default:
                break;
        }
    }

    public void changeLocale(String locale) {
        Resources res = getActivity().getResources();
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.setLocale(new Locale(locale.toLowerCase())); // API 17+ only.
        // Use conf.locale = new Locale(...) if targeting lower versions
        res.updateConfiguration(conf, dm);
    }
}